# Changelog<br>


<a name="budge-0.0.2"></a>
### [budge-0.0.2](https://github.com/truecharts/apps/compare/budge-0.0.1...budge-0.0.2) (2022-02-28)

#### Chore

* update image ([#1985](https://github.com/truecharts/apps/issues/1985))



<a name="budge-0.0.1"></a>
### budge-0.0.1 (2022-02-28)

#### Feat

* Add budge ([#1976](https://github.com/truecharts/apps/issues/1976))
