# Changelog<br>


<a name="deemix-0.0.1"></a>
### deemix-0.0.1 (2022-02-25)

#### Feat

* Add deemix ([#1933](https://github.com/truecharts/apps/issues/1933))
